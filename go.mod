module gitlab.com/project_falcon/kubeless/klib/secretlego

go 1.14

require (
	k8s.io/api v0.19.4
	k8s.io/apimachinery v0.19.4
)
