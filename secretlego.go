package secretlego

import (
	apiV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	IDRsa = "falcon-idrsa"
)

func GetSecret(name string, data map[string][]byte) apiV1.Secret {
	secret := apiV1.Secret{
		ObjectMeta: metaV1.ObjectMeta{
			Name:      name,
			// Namespace: namespace,
		},
		Type: apiV1.SecretTypeOpaque,
		Data: data,
	}

	return secret
}
